# Importing libraries
import csv
import matplotlib.pyplot as plt
import pandas as pd

# Importing data from csv file
csv_pandas = pd.read_csv('crime_dataset.csv')
csv_pandas = csv_pandas[3:23]

# Variables
dataset = []
list_of_years = []
list_of_population = []
list_of_rape = []

# From row to list
for row in csv_pandas.iterrows():
    index, data = row
    dataset.append(data.tolist())

# Saving data into differents lists
for item in dataset:
	list_of_years.append(float(item[0]))
	list_of_population.append(float(item[1].replace(',','')))
	list_of_rape.append(float(item[8].replace(',','')))


# Plotting population
pop = plt.figure(1)
plt.plot(list_of_years, list_of_population)
plt.title("Population in USA")
plt.xlabel("Year")
plt.ylabel("Population")
pop.show()

# Plotting number of rape
rape = plt.figure(2)
plt.plot(list_of_years, list_of_rape)
plt.title("Rape in USA")
plt.xlabel("Year")
plt.ylabel("Rape")
rape.show()

raw_input()